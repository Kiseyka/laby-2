#include <iostream>
#include <chrono>
#include <timer.hpp>
#define N_Max 10000

//Дана последовательности из N чисел. 
//Если их сумма больше 1000, отсортируйте по возрастанию.

void BUBBLESORT(int mas[N_Max], int n)
{
	for (int i = 1; i < n; i++)
	{
		if (mas[i] >= mas[i - 1])
			continue;
		int j = i - 1;
		while (j >= 0 && mas[j] > mas[j + 1])
		{
			std::swap(mas[j], mas[j + 1]);
			j--;
		}
	}
}

int main()
{
	int N;
	std::cin >> N;
	int k = 0;
	int mas[N_Max];
	for (int i = 0; i < N; i++) 
	{
		std::cin >> mas[i];
		k += mas[i];
	}
	srand(time(0));
	Timer t;
	if (k > 1000)
		BUBBLESORT(mas, N);

	for (int i = 0; i < N; i++)
		std::cout << mas[i] << std::endl;

	std::cout << "Time elapsed: " << t.elapsed() << '\n';
	return 0;
}

