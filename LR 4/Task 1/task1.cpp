#include "time.hpp"
// Вариант 2

struct T_List
{
	T_List* next;
	int num;
};

void PRINT(T_List* head)
{
	T_List* p = head->next;
	while (p != nullptr)
	{
		std::cout << p->num << std::endl;
		p = p->next;
	}
}

void ADD(T_List* head, int num)
{
	T_List* p = new T_List;
	p->num = num;
	p->next = head->next;
	head->next = p;
}

void CLEAR(T_List* head)
{
	T_List* tmp;
	T_List* p = head->next;
	while (p != nullptr)
	{
		tmp = p;
		p = p->next;
		delete tmp;
	}
}

void DELETE(T_List* head, int k)
{
	T_List* tmp;
	T_List* p = head;
	while (p->next != nullptr)
	{
		if (p->next->num == k)
		{
			tmp = p->next;
			p->next = p->next->next;
			delete tmp;
		}
		else
			p = p->next;
	}
}

int main()
{
	setlocale(LC_ALL, "Rus");
	std::cout << "Введите элемент, который нужно удалить: "  << std::endl;
	int k;
	std::cin >> k;
	T_List* head = new T_List; // Создаём список
	head->next = nullptr;

	for (int i = 0; i < N; i++) // Заполняем список
		ADD(head, i);

	Timer t; // Включаем таймер

	for (int i = 0; i < M; i++) // Удаляем элементы
		DELETE(head, k);
	//PRINT(head); // Выводим результат (для проверки)

	std::cout << "Time: " << t.elapsed(); // Выводим время

	CLEAR(head); // Чистим
	delete head;
	return 0;
}
