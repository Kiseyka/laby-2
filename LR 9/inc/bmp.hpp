#pragma once
#include <string>
#include <fstream>
#include <mathutils/matrix.hpp>

namespace ek
{
#pragma pack(1)
    struct Pixel
    {
        unsigned char b;
        unsigned char g;
        unsigned char r;
    };
#pragma pack()

    class BMP {
    public:
        void Read();
        void Write();
        void Clean();
        void BlueFilter();
    private:
        int m_width, m_height;
        Pixel** m_pixels;

#pragma pack(1) 
        struct BMPHEADER
        {
            unsigned short    Type;
            unsigned int      Size;
            unsigned short    Reserved1;
            unsigned short    Reserved2;
            unsigned int      OffBits;
        };
#pragma pack()
        BMPHEADER bmpHeader{};
#pragma pack(1)
        struct BMPINFO
        {
            unsigned int    Size;
            int             Width;
            int             Height;
            unsigned short  Planes;
            unsigned short  BitCount;
            unsigned int    Compression;
            unsigned int    SizeImage;
            int             XPelsPerMeter;
            int             YPelsPerMeter;
            unsigned int    ClrUsed;
            unsigned int    ClrImportant;
        };
        BMPINFO bmpInfo{};
#pragma pack()
    };
}
 