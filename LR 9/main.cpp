﻿#include <bmp.hpp>
#include <fstream>

int main()
{
	ek::BMP bmp_Image;
	bmp_Image.Read();
	bmp_Image.BlueFilter();
	bmp_Image.Write();
	bmp_Image.Clean();
	return 0;
}
