﻿#include <iostream>

class Matrix
{
public:
	// Конструктор
	Matrix(int n, int m)
	{
		//std::cout << "Constructor" << std::endl;
		m_n = n;
		m_m = m;
		m_mat = new int* [m_n];
		for (int i = 0; i < m_n; i++)
			m_mat[i] = new int[m_m];
	}

	// Конструктор копирования
	Matrix(const Matrix& mat)
	{
		//std::cout << "Copy constructor" << std::endl;

		m_n = mat.m_n;
		m_m = mat.m_m;

		m_mat = new int* [m_n];
		for (int i = 0; i < m_n; i++)
			m_mat[i] = new int[m_m];

		for (int i = 0; i < m_n; i++)
			for (int j = 0; j < m_m; j++)
				m_mat[i][j] = mat.m_mat[i][j];
	}

	// Присваивание
	Matrix& operator=(const Matrix& mat)
	{
		std::cout << "Operator =" << std::endl;

		m_n = mat.m_n;
		m_m = mat.m_m;

		for (int i = 0; i < m_n; i++)
			for (int j = 0; j < m_m; j++)
				m_mat[i][j] = mat.m_mat[i][j];

		return *this;
	}

	int getN() const { return m_n; }
	int getM() const { return m_m; }
	int get(int i, int j) const { return m_mat[i][j]; }
	void set(int i, int j, int data) { m_mat[i][j] = data; }

	// Оператор сложения
	Matrix operator+(const Matrix& mat) {
		std::cout << "operator+" << std::endl;
		Matrix tmp(2, 3);
		for (int i = 0; i < m_n; i++)
			for (int j = 0; j < m_m; j++)
				tmp.m_mat[i][j] = m_mat[i][j] + mat.m_mat[i][j];
		return tmp;
	}
	// Оператор вычитания
	Matrix operator-(const Matrix& mat) {
		std::cout << "operator-" << std::endl;
		Matrix tmp(2, 3);
		for (int i = 0; i < m_n; i++)
			for (int j = 0; j < m_m; j++)
				tmp.m_mat[i][j] = m_mat[i][j] - mat.m_mat[i][j];
		return tmp;
	}

	// Оператор вычитания  C -= A <=> C = C - A
	Matrix operator-=(const Matrix& mat) {
		std::cout << "operator-=" << std::endl;
		Matrix tmp(2, 3);
		for (int i = 0; i < m_n; i++)
			for (int j = 0; j < m_m; j++)
				tmp.m_mat[i][j] = m_mat[i][j] - mat.m_mat[i][j];
		return tmp;
	}

	// Оператор сложения  C += A <=> C = C + A
	Matrix operator+=(const Matrix& mat) {
		std::cout << "operator+=" << std::endl;
		Matrix tmp(2, 3);
		for (int i = 0; i < m_n; i++)
			for (int j = 0; j < m_m; j++)
				tmp.m_mat[i][j] = m_mat[i][j] + mat.m_mat[i][j];
		return tmp;
	}

	// Оператор умножения
	Matrix operator*(const Matrix& mat) {
		std::cout << "operator*" << std::endl;
		Matrix tmp(2, 3);
		for (int i = 0; i < m_n; i++)
			for (int j = 0; j < mat.getM(); j++)
			{
				int sum = 0;
				for (int k = 0; k < m_m; k++)
					sum += m_mat[i][k] * mat.get(k, j);
				tmp.set(i, j, sum);
			}

		return tmp;
	}

	// Деструктор
	~Matrix()
	{
		//std::cout << "Destructor" << std::endl;
		for (int i = 0; i < m_n; i++)
			delete[] m_mat[i];
		delete m_mat;
	}
	// Определитель матрицы
	int Matrix::Det()
	{
		if (m_n == 2)
			return (m_mat[0][0] * m_mat[1][1] - m_mat[1][0] * m_mat[0][1]);
		else if (m_n == 3)
			return (m_mat[0][0] * m_mat[1][1] * m_mat[2][2] + 
				m_mat[0][1] * m_mat[1][2] * m_mat[2][0] + 
				m_mat[0][2] * m_mat[1][0] * m_mat[2][1] -
				m_mat[0][2] * m_mat[1][1] * m_mat[2][0] -
				m_mat[0][0] * m_mat[1][2] * m_mat[2][1] - 
				m_mat[2][2] * m_mat[0][1] * m_mat[1][0]);
	}
	// Обратная матрица
	Matrix Matrix::Inverse()
	{
		Matrix inv_mat(m_n, m_m);

		for (int i = 0; i < m_n; i++)
			for (int j = 0; j < m_m; j++)
				inv_mat.m_mat[i][j] = 0;
				
		if (m_n == 2)
		{
			inv_mat.m_mat[0][0] = m_mat[1][1] / Det();
			inv_mat.m_mat[0][1] = -m_mat[1][0] / Det();
			inv_mat.m_mat[1][0] = -m_mat[0][1] / Det();
			inv_mat.m_mat[1][1] = m_mat[0][0] / Det();
			return inv_mat;
		}
		else if (m_n == 3)
		{
			inv_mat.m_mat[0][0] = (m_mat[1][1] * m_mat[2][2] - m_mat[2][1] * m_mat[1][2]) / Det();
			inv_mat.m_mat[0][1] = -(m_mat[0][1] * m_mat[2][2] - m_mat[2][1] * m_mat[0][2]) / Det();
			inv_mat.m_mat[0][2] = (m_mat[0][1] * m_mat[1][2] - m_mat[1][1] * m_mat[0][2]) / Det();
			inv_mat.m_mat[1][0] = -(m_mat[1][0] * m_mat[2][2] - m_mat[2][0] * m_mat[1][2]) / Det();
			inv_mat.m_mat[1][1] = (m_mat[0][0] * m_mat[2][2] - m_mat[2][0] * m_mat[0][2]) / Det();
			inv_mat.m_mat[1][2] = -(m_mat[0][0] * m_mat[1][2] - m_mat[1][0] * m_mat[0][2]) / Det();
			inv_mat.m_mat[2][0] = (m_mat[1][0] * m_mat[2][1] - m_mat[2][0] * m_mat[1][1]) / Det();
			inv_mat.m_mat[2][1] = -(m_mat[0][0] * m_mat[2][1] - m_mat[2][0] * m_mat[0][1]) / Det();
			inv_mat.m_mat[2][2] = (m_mat[0][0] * m_mat[1][1] - m_mat[1][0] * m_mat[0][1]) / Det();
			return inv_mat;
		}
	}
	// Транспонированная матрица
	Matrix Matrix::Transp()
	{
		if (m_n == m_m)
		{
			Matrix trn_mat(m_n, m_m);
			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					trn_mat.m_mat[i][j] = m_mat[j][i];
			return trn_mat;
		}
		else
		{
			Matrix trn_mat(m_m, m_n);
			for (int i = 0; i < m_m; i++)
				for (int j = 0; j < m_n; j++)
					trn_mat.m_mat[i][j] = m_mat[j][i];
			return trn_mat;
		}
	}

	// friend - позволяет функции иметь доступ к private полям/методам класса
	friend std::istream& operator>>(std::istream& os, Matrix& mat);
	friend std::ostream& operator<<(std::ostream& os, const Matrix& mat);

	// Использование внутри класса
private:
	int m_n, m_m;		// Поле
	int n;
	int** m_mat;
};

// Перегрузка оператора ввода
std::istream& operator>>(std::istream& in, Matrix& mat)
{
	for (int i = 0; i < mat.m_n; i++)
		for (int j = 0; j < mat.m_m; j++)
			in >> mat.m_mat[i][j];
	return in;
}

// Перегрузка оператора вывода
std::ostream& operator<<(std::ostream& out, const Matrix& mat)
{
	out << "Matrix " << mat.m_n << "x" << mat.m_m << std::endl;
	for (int i = 0; i < mat.m_n; i++) {
		for (int j = 0; j < mat.m_m; j++)
			out << mat.m_mat[i][j] << " ";
		out << std::endl;
	}
	return out;
}

void Printt(Matrix Matrix_1)
{
	std::cout << "Определитель матрицы = " << Matrix_1.Det() << std::endl;
	if (Matrix_1.Det() != 0)
		std::cout <<  "Обратная матрица = " << Matrix_1.Inverse() << std::endl;
	else
		std::cout << "Обратная матрица не существует" << std::endl;
	std::cout << "Транспонированная матрица = " << Matrix_1.Transp() << std::endl;
}

int main()
{
	setlocale(LC_ALL, "Rus");
	std::cout << "Вычисление определителя и нахождение обратной матрицы не поддерживается для матриц порядком > 3" << std::endl;
	std::cout << "Введите размерность матрицы: " << std::endl;
	int a, b;
	std::cin >> a >> b;
	if (a <= 3 && b <= 3)
	{
		std::cout << "Введите матрицу "  << a << "x" << b << ":" << std::endl;
		Matrix Matrix_1(a, b);
		std::cin >> Matrix_1;
		if ((a == b && a == 2) || (a == b && a == 3))
			Printt(Matrix_1);
		else
		{
			std::cout << "Определитель матрицы не существует "<< std::endl;
			std::cout << "Обратная матрица не существует" << std::endl;
			std::cout << "Транспонированная матрица = " << Matrix_1.Transp() << std::endl;
		}
	}
	else
	{
		std::cout << "Операции вычисления определителя и нахождения обратной матрицы не поддерживаются для матрицы такого порядка"<< std::endl;
		Matrix Matrix_1(a, b);
		std::cin >> Matrix_1;
		std::cout << "Транспонированная матрица = " << Matrix_1.Transp() << std::endl;
	}
	return 0;
}