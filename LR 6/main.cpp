#include <iostream>
#include <fstream>
#define N 100

struct Elem
{
    int data; 
    Elem* left;
    Elem* right;
    Elem* parent;
};
Elem* MAKE(int data, Elem* p)
{
    Elem* q = new Elem;          
    q->data = data;
    q->left = nullptr;
    q->right = nullptr;
    q->parent = p;
    return q;
}

void PUSH(int data, Elem*& root)
{
    if (root == nullptr) {
        root = MAKE(data, nullptr);
        return;
    }
    Elem* v = root;
    while ((data < v->data && v->left != nullptr) || (data > v->data && v->right != nullptr))
        if (data < v->data)
            v = v->left;
        else
            v = v->right;
    if (data == v->data)
        return;
    Elem* u = MAKE(data, v);
    if (data < v->data)
        v->left = u;
    else
        v->right = u;
}

void PASS(Elem* v)
{
    if (v == nullptr)
        return;
    PASS(v->left);
    std::cout << v->data << std::endl;

    PASS(v->right);
}

Elem* SEARCH(int data, Elem* v)
{
    if (v == nullptr)
        return v;
    if (v->data == data)
        return v;
    if (data < v->data)
        return SEARCH(data, v->left);
    else
        return SEARCH(data, v->right);
}

void CLEAR(Elem*& v)
{
    if (v == nullptr)
        return;
    CLEAR(v->left);
    CLEAR(v->right);
    delete v;
    v = nullptr;
}
int DEEP(Elem* v, int num, int deep)
{
    if (v == nullptr)
        return 0;
        if (v->data == num)
            return deep;
        if (v->data > num)
            return DEEP(v->left, num, deep + 1);
        else
            return DEEP(v->right, num, deep + 1);
}

int main()
{
    setlocale(LC_ALL, "Rus");
    int sr;
    int num;
    int check = 0;
    std::cout << "Введите элемент, который нужно найти" << std::endl;
    std::cin >> sr;
    std::ifstream in("input.txt");
    Elem* root = nullptr;
    while (!in.eof())
    {
        in >> num;
        PUSH(num, root);
        if (num == 0)
            CLEAR(root);
        if ((SEARCH(num, root) != 0) && (num == sr))
        {
            std::cout << "Элемент найден, глубина: ";
            std::cout <<DEEP(root, sr, 1);
            check = 1;
        }
    }
    if (check == 0)
        std::cout << "Элемент не найден :с" << std::endl;
    CLEAR(root);
    return 0;
}
