#include <iostream>
#include <fstream>
#define N 1000000000

struct Elem
{
    int data; 
    Elem* left;
    Elem* right;
    Elem* parent;
};
Elem* MAKE(int data, Elem* p)
{
    Elem* q = new Elem;          
    q->data = data;
    q->left = nullptr;
    q->right = nullptr;
    q->parent = p;
    return q;
}

void ADD(int data, Elem*& root)
{
    if (root == nullptr) {
        root = MAKE(data, nullptr);
        return;
    }
    Elem* v = root;
        while ((data < v->data && v->left != nullptr) || (data > v->data && v->right != nullptr))
            if (data < v->data)
                v = v->left;
            else
                v = v->right;
    if (data == v->data)
        return;
    Elem* u = MAKE(data, v);
    if (data < v->data)
        v->left = u;
    else
        v->right = u;
} 
void PASS(Elem* v)
{
    if (v == nullptr)
        return;
    PASS(v->left);
    std::cout << v->data << std::endl;

    PASS(v->right);
}
Elem* SEARCH(int data, Elem* v)
{
    if (v == nullptr)
        return v;
    if (v->data == data)
        return v;
    if (data < v->data)
        return SEARCH(data, v->left);
    else
        return SEARCH(data, v->right);
}
void DELETE(int data, Elem*& root)
{ 
    Elem* u = SEARCH(data, root);
    if (u == nullptr)
        return;
    if (u->left == nullptr && u->right == nullptr && u == root)
    {
        delete root;
        root = nullptr;
        return;
    }   
    if (u->left == nullptr && u->right != nullptr && u == root)
    {
        Elem* t = u->right;
        while (t->left != nullptr)
            t = t->left;
        u->data = t->data;
        u = t;
    } 
    if (u->left != nullptr && u->right == nullptr && u == root)
    {
        Elem* t = u->left;
        while (t->right != nullptr)
            t = t->right;
        u->data = t->data;
        u = t;
    } 
    if (u->left != nullptr && u->right != nullptr)
    {
        Elem* t = u->right;
        while (t->left != nullptr)
            t = t->left;
        u->data = t->data;
        u = t;
    }
    Elem* t;
    if (u->left == nullptr)
        t = u->right;
    else
        t = u->left;
    if (u->parent->left == u)
        u->parent->left = t;
    else
        u->parent->right = t;
    if (t != nullptr)
        t->parent = u->parent;
    delete u;
}
void CLEAR(Elem*& v)
{
    if (v == nullptr)
        return;

    CLEAR(v->left);

    CLEAR(v->right);

    delete v;
    v = nullptr;
}
int DEEP(Elem* v, int num, int deep)
{
    if (v == nullptr)
        return 0;
    if (v->data == num)
        return deep;
    if (num < v->data)
        return DEEP(v->left, num, deep + 1);
    else
        return DEEP(v->right, num, deep + 1);
}

int main()
{
    char sym;
    int num;
    Elem* root = nullptr;
    std::ifstream in("input.txt");
    std::ofstream out("output.txt");

    while (!in.eof())
    {
        in >> sym >> num;
        if (sym == '+')
            ADD(num, root);
        if (sym == '-')
            DELETE(num, root);
        if (sym == '?')
            if (SEARCH(num, root) == 0)
                out << 'n';
            else out << DEEP(root, num, 1);
        if (sym == 'E')
            break;
    }
    CLEAR(root);
    return 0;
}
