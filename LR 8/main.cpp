#include <iostream>
#include <cassert>
#include "inc/matrix.hpp"

using ek::Vec2d;
using ek::Vec2i;
using ek::Mat22d;
using ek::Mat22i;
using ek::Mat33d;
using ek::Mat33i;

int main()
{
	std::cout << "=== Test 1 ===" << std::endl;
	std::cout << "matrix 2x2 determinant" << std::endl;
	{
		Mat22d A({ {
			 {1.1,2.6},
			 {3.3,4.2}
		} });
		try
		{
			std::cout << A.Det() << std::endl;
			std::cout << "After det in main" << std::endl;
		}
		catch (const std::bad_alloc& e)
		{
			std::cerr << "BAD ALLOC!!!! " << e.what() << std::endl;
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << std::endl;
		}
		std::cout << std::endl;

		std::cout << "matrix 2x1 determinant" << std::endl;
		Vec2d B({ {
			{1.2},
			{1.5}
		} });
		try
		{
			std::cout << B.Det() << std::endl;
			std::cout << "After det in main" << std::endl;
		}
		catch (const std::bad_alloc& e)
		{
			std::cerr << "BAD ALLOC!!!! " << e.what() << std::endl;
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << std::endl;
		}
		std::cout << std::endl;

		std::cout << "matrix 3x3 determinant" << std::endl;
		Mat33i C({ {
			 {1,2,6},
			 {3,4,2},
			 {4,1,11}
		} });
		try
		{
			std::cout << C.Det() << std::endl;
			std::cout << "After det in main" << std::endl;
		}
		catch (const std::bad_alloc& e)
		{
			std::cerr << "BAD ALLOC!!!! " << e.what() << std::endl;
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << std::endl;
		}
	}
	std::cout << "Done!" << std::endl;
	std::cout << std::endl;
	std::cout << "=== Test 2 ===" << std::endl;
	std::cout << "inverse matrix to 2x2 matrix" << std::endl;
	{
		Mat22d A({ {
			 {1.1,2.6},
			 {3.3,4.2}
		} });
		try
		{
			std::cout << A.inv_mat() << std::endl;
			std::cout << "After inv in main" << std::endl;
		}
		catch (const std::bad_alloc& e)
		{
			std::cerr << "BAD ALLOC!!!! " << e.what() << std::endl;
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << std::endl;
		}
	}

	std::cout << "inverse matrix to 2x1 matrix" << std::endl;
	{
		Vec2d B({ {
			 {1.1},
			 {3.34}
		} });
		try
		{
			std::cout << B.inv_mat() << std::endl;
			std::cout << "After inv in main" << std::endl;
		}
		catch (const std::bad_alloc& e)
		{
			std::cerr << "BAD ALLOC!!!! " << e.what() << std::endl;
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << std::endl;
		}
	}

	std::cout << "inverse matrix to 3x3 matrix" << std::endl;
	{
		Mat33d C({ {
			{1,2,5},
			{3,4,5},
			{1,4,9}
	} });
		try
		{
			std::cout << C.inv_mat() << std::endl;
			std::cout << "After inv in main" << std::endl;
		}
		catch (const std::bad_alloc& e)
		{
			std::cerr << "BAD ALLOC!!!! " << e.what() << std::endl;
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << std::endl;
		}
	}
	std::cout << "Done!" << std::endl;
	std::cout << std::endl;
	std::cout << "=== Test 3 ===" << std::endl;
	std::cout << "matrix transposition 3x3" << std::endl;
	{
		Mat33d A({ {
			 {1,2,3},
			 {4,5,6},
			 {7,8,9}
		} });

		try
		{
			std::cout << A.Transp() << std::endl;
			std::cout << "After transp in main" << std::endl;
		}
		catch (const std::bad_alloc& e)
		{
			std::cerr << "BAD ALLOC!!!! " << e.what() << std::endl;
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << std::endl;
		}
	}

	std::cout << "matrix transposition 2x1" << std::endl;
	Mat22i B({ {
		{1,2},
		{1,5}
	} });
	try
	{
		std::cout << B.Transp() << std::endl;
		std::cout << "After transp in main" << std::endl;
	}
	catch (const std::bad_alloc& e)
	{
		std::cerr << "BAD ALLOC!!!! " << e.what() << std::endl;
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
	std::cout << std::endl;
	std::cout << "Done!" << std::endl;
	return 0;
}
